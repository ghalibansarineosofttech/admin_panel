import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Badge from '@material-ui/core/Badge';
// import ButtonComponent from './ButtonComponent'


//Style's.
const customStyles = {
  header: {
      display: 'flex',
      justifyContent: 'space-between'
  },
  maindiv: {
    //   width: '100%',
      display: 'flex',
      justifyContent: 'space-between'
  },
  subdiv: {display: 'flex'}
};


//Navigation.
class Navigation extends Component {
    render() {
        return (
            <AppBar position="static">
                <Toolbar style={customStyles.maindiv}>
                    <div style={{display: 'flex'}}>
                            <Typography variant="h4" >Neo</Typography>
                            <Typography variant="h4" style={{color: 'yellow'}}>Store</Typography>
                    </div>
                    <div>
                        <div style={{display: 'flex', justifyContent: 'space-between'}}>
                            {/*
                            //@ts-ignore */}
                            <Button onClick={()=>{this.props.history.push('/')}} style={{color: 'white'}}>Home</Button>
                            {/*
                            //@ts-ignore */}
                            <Button onClick={()=>{this.props.history.push('/productpage')}} style={{color: 'white'}}>Products</Button>
                            {/*
                            //@ts-ignore */}
                            <Button onClick={()=>{this.props.history.push('/productdetailpage/5cfe41a5b4db0f338946eac3')}} style={{color: 'white'}}>Order</Button>
                        </div>
                    </div>
                    <div style={{display: "flex"}}>
                        <div style={{display: "flex", background: 'white'}}>
                            <SearchIcon style={{color:'black'}} />
                            <InputBase
                                style={{color: 'black'}}
                                placeholder="Search…"
                                inputProps={{ 'aria-label': 'search' }}
                            />
                        </div>
                        <Button style={{color:'white'}}><Badge badgeContent={4} color="secondary"><ShoppingCartIcon fontSize='large' /></Badge> Cart</Button>
                        {/*
                        //@ts-ignore */}
                        <Button style={{color: 'white'}} onClick={()=>{this.props.history.push('/login')}}>Login</Button>
                        {/*
                        //@ts-ignore */}
                        <Button style={{color: 'white'}} onClick={()=>{this.props.history.push('/registration')}}>Register</Button>
                    </div>
                </Toolbar>
            </AppBar>
        );
    }
}


//@ts-ignore
export default (withRouter(Navigation))