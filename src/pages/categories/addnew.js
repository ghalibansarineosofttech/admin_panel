import React, {Component} from 'react';
import Grid from "@material-ui/core/Grid";
import {Paper, TextField} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import ColorPicker from 'material-ui-color-picker'
import {Link} from 'react-router-dom';
import {Constants} from '../../helpers/constant'


//style
const customStyle = {
    mainDiv: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    div: {
        // height: 750,
        width: 450,
        marginTop: 20,
        paddingTop: 10,
        paddingBottom: 20,
        marginBottom: 20,
        borderColor: 'black',
        textAlign: 'center'
    },
    input: {width: '70%'}
};


class Addnew extends Component {
    state = {
        categoryId: '',
        categoryName: '',
        categoryType: '',
        description: '',
        programs: [],
        categoryBgColor: '',
        CategoryTextColor: '',
        errors: {
            categoryIdErr: '',
            categoryNameErr: '',
            categoryTypeErr: '',
            descriptionErr: '',
            programsErr: '',
            categoryBgColorErr: '',
            CategoryTextColorErr: '',
        }
    };

    componentDidMount() {
        if (this.props.location.editCategory) {
            const {categoryId, categoryName, categoryType,description, programs, categoryBgColor, CategoryTextColor} = this.props.location.editCategory;
            this.setState({categoryId, categoryName, categoryType,description, programs, categoryBgColor, CategoryTextColor});
        }
    }

    inputChange = async (event, inputType) => {
        if(inputType === 'programs'){
            await this.setState({[inputType]: event.target.value.split(',')});
        }else {
            await this.setState({[inputType]: event.target.value});
        }
    };

    colorChange = async (event, inputType) => {
        if(event !== undefined){
            await this.setState({[inputType]: event});
        }
    };

    addCategory = async () => {
        const nameReg = Constants.nameReg;
        const numberReg = Constants.numberReg;
        const hexReg = Constants.hexReg;
        const {categoryId, categoryName, categoryType,description, programs, categoryBgColor, CategoryTextColor} = this.state;
        await this.setState((prevState) => {
            let errors = {...prevState.errors};
            if (!nameReg.test(categoryName)) { errors.categoryNameErr = Constants.nameErr }
            else { errors.categoryNameErr = Constants.blank }
            if (categoryType === '') { errors.categoryTypeErr = Constants.required}
            else if (!numberReg.test(categoryType)) { errors.categoryTypeErr = Constants.numberErr }
            else { errors.categoryTypeErr = Constants.blank }
            if (description === '') { errors.descriptionErr = Constants.required }
            else { errors.descriptionErr = Constants.blank }
            if (programs === '') { errors.programsErr = Constants.required}
            else if (!numberReg.test(programs)) { errors.programsErr = Constants.numberErr }
            else { errors.programsErr = Constants.blank }
            if (!hexReg.test(categoryBgColor)) { errors.categoryBgColorErr = Constants.hexErr }
            else { errors.categoryBgColorErr = Constants.blank }
            if (!hexReg.test(CategoryTextColor)) { errors.CategoryTextColorErr = Constants.hexErr }
            else { errors.CategoryTextColorErr = Constants.blank }
            return {errors}
        });
        const {categoryNameErr, categoryTypeErr, descriptionErr, programsErr, categoryBgColorErr, CategoryTextColorErr} = this.state.errors
        if(!categoryNameErr && !categoryTypeErr && !descriptionErr && !programsErr && !categoryBgColorErr && !CategoryTextColorErr){
            this.props.history.push({pathname: '/', newCategory: {categoryId, categoryName, categoryType,description, programs, categoryBgColor, CategoryTextColor}});
        }
    };

    render() {
        const {categoryName, categoryType, description, programs, categoryBgColor, CategoryTextColor} = this.state;
        const {categoryNameErr, categoryTypeErr, descriptionErr, programsErr, categoryBgColorErr, CategoryTextColorErr} = this.state.errors
        return (
            <Grid style={customStyle.mainDiv}>
                <Grid xs={12} sm={10} md={6} style={{display: 'flex', justifyContent: 'center'}} >
                    <Paper elevation={4} style={customStyle.div}>
                        <h1>Add New Category.</h1>
                        <TextField
                            required
                            id=""
                            helperText={categoryNameErr}
                            error={!!categoryNameErr}
                            label="Category Name"
                            type="text"
                            value={categoryName}
                            // autoComplete="firstName"
                            style={customStyle.input}
                            onChange={event => this.inputChange(event, 'categoryName')}
                        />
                        <br/><br/>
                        <TextField
                            required
                            helperText={categoryTypeErr}
                            error={!!categoryTypeErr}
                            label="Category Type"
                            type="text"
                            value={categoryType}
                            style={customStyle.input}
                            onChange={event => this.inputChange(event, 'categoryType')}
                        />
                        <br/><br/>
                        <TextField
                            required
                            id=""
                            helperText={descriptionErr}
                            error={!!descriptionErr}
                            label="Description"
                            type="text"
                            value={description}
                            // autoComplete="firstName"
                            style={customStyle.input}
                            onChange={event => this.inputChange(event, 'description')}
                        />
                        <br/><br/>
                        <TextField
                            required
                            id=""
                            helperText={programsErr}
                            error={!!programsErr}
                            label="Programs"
                            type="text"
                            value={programs}
                            style={customStyle.input}
                            onChange={event => this.inputChange(event, 'programs')}
                        />
                        <br/><br/>
                        <TextField
                            label='Category Bg Color'
                            helperText={categoryBgColorErr}
                            error={!!categoryBgColorErr}
                            style={{width: '45%'}}
                            value={categoryBgColor}
                        />
                        <ColorPicker
                            id='categoryBgColor'
                            name='color'
                            label='Click here'
                            style={{width: '25%'}}
                            onChange={color => this.colorChange(color, 'categoryBgColor')}
                        />
                        <br/><br/>
                        <TextField
                        label='Category Text Color'
                        style={{width: '45%'}}
                        helperText={CategoryTextColorErr}
                        error={!!CategoryTextColorErr}
                        value={CategoryTextColor}
                        color={CategoryTextColor}
                        />
                        <ColorPicker
                            name='color'
                            label="Click here"
                            style={{width: '25%'}}
                            onChange={color => this.colorChange(color, 'CategoryTextColor')}
                        />
                        <br/><br/>
                        <Button variant="contained" color="primary" onClick={()=>{this.addCategory()}}>Submit</Button>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

export default Addnew;