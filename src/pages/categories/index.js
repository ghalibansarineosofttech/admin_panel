import React, {Component} from 'react'
import axios from 'axios'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {Button, Grid} from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import {Link} from "react-router-dom";


const style = {
    table: {
        // minWidth: 650,
        width: '100%'
    },
}

export default class categories extends Component {
    state = {
        categories: []
    };

    componentDidMount(){
        axios.get('https://d13moeh3wl3uva.cloudfront.net/programs_v11_en.json')
        .then(val => { this.setState({categories: val.data.categories}) })
        .then(() => {
            if (this.props.location.newCategory) {
                let temp = this.props.location.newCategory;
                console.log(temp,"temp");
                let tempCategory = {};
                if(temp.categoryId){
                    this.setState((prevState) => {
                        let categories = [...prevState.categories];
                        categories[temp.categoryId-1].categoryName= temp.categoryName;
                        categories[temp.categoryId-1].categoryType = temp.categoryType;
                        categories[temp.categoryId-1].description = temp.description;
                        categories[temp.categoryId-1].programs = temp.programs;
                        categories[temp.categoryId-1].uiData = {categoryBgColor: temp.categoryBgColor, categoryTextColor: temp.CategoryTextColor};
                        return {categories}
                    });
                }
                else {
                    tempCategory.categoryId = 5;
                    tempCategory.categoryName = temp.categoryName;
                    tempCategory.categoryType = temp.categoryType;
                    tempCategory.description = temp.description;
                    tempCategory.programs = temp.programs;
                    tempCategory.uiData = {categoryBgColor: temp.categoryBgColor, categoryTextColor: temp.CategoryTextColor};
                    this.setState((prevState) => {
                        let categories = [...prevState.categories];
                        categories.push(tempCategory);
                        return {categories}
                    });
                }
            }
        })
    };

    deleteCategory = (index) => {
        console.log("index",index);
        // delete(this.state.categories[index]);
        this.setState((prevState) => {
            let categories = [...prevState.categories];
            categories.splice(index,1)
            // delete categories[index];
            // categories = categories.filter((value) => { console.log(index,value,index !== value); return index !== value } )
            console.log(categories,"categories1");
            return {categories}
        });
        // console.log(this.state.categories,"categories");
    };

    editCategory = (index) => {
        const {categoryId, categoryName, categoryType,description, programs, uiData} = this.state.categories[index];
        this.props.history.push({pathname: '/addnew', editCategory: {categoryId, categoryName, categoryType, description, programs, categoryBgColor: uiData.categoryBgColor, CategoryTextColor: uiData.categoryTextColor}})
    };

    tablebodydata = () => {
        const {categories} = this.state;
        return categories.map((row, index) => {
            const {categoryId, categoryType, categoryName} = row;
            return(
                <TableRow key={categoryId}>
                    <TableCell component="th" scope="row">{categoryId}</TableCell>
                    <TableCell >{categoryName}</TableCell>
                    <TableCell >{categoryType}</TableCell>
                    <TableCell ><Button onClick={()=>{this.editCategory(index)}} variant="contained" color="primary" size='small'>Edit</Button></TableCell>
                    <TableCell ><Button onClick={()=>{this.deleteCategory(index)}} variant="contained" color="secondary" size='small'>Delete</Button></TableCell>
                </TableRow>
            )
        })
    }

    render() {
        const {categories} = this.state;
        return (
            <Grid xs={12} sm={11} md={12} style={{display: 'flex', justifyContent: 'center', flexDirection: 'Column'}}>
                <Grid xs={12} sm={11} md={10} style={{display: 'flex', justifyContent: 'center'}}>
                    <Grid xs={12} sm={6}><h1>Categories</h1></Grid>
                    <Grid xs={12} sm={6}><Button variant='contained' color='primary'><Link to='/addnew' style={{color: 'white', textDecoration:'none'}}>Add</Link></Button></Grid>
                </Grid>
                <Grid xs={12} style={{display: 'flex', justifyContent: 'center', paddingBottom: '30px'}}>
                    <Grid xs={12} sm={11} md={10}>
                    <Paper elevation={4} >
                    <TableContainer>
                        <Table aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <TableCell>#</TableCell>
                                <TableCell>Name</TableCell>
                                <TableCell>Category Type</TableCell>
                                <TableCell>Edit</TableCell>
                                <TableCell>Delete</TableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                                {categories.length===0 ? "No data available" : this.tablebodydata() }
                            </TableBody>
                        </Table>
                    </TableContainer>
                    </Paper>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}
