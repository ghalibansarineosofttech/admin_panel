import React, { Component } from 'react'
import logo from './logo.svg';
import './App.css';
import {Grid} from '@material-ui/core';
import {BrowserRouter as Router, Redirect, Route, Switch, Link} from 'react-router-dom';
import sideBarz from './pages/sidebar'
import Navigation from './pages/menubar'
import categories from './pages/categories'
import programs from './pages/programs'
import bodyArea from './pages/bodyarea'
import products from './pages/products'
import Addnew from './pages/categories/addnew'

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';


class App extends Component {
  render() {
    return (
      <Router>
        {/* <Navigation {...this.props} /> */}
        <Grid xs={12} style={{display: 'flex', paddingTop: '15px', justifyContent: 'center'}}>
          <Grid xs={12} sm={4} md={3} style={{display: 'flex', justifyContent: 'center'}}>
            <Grid xs={11} md={11} >
              <Paper elevation={3} style={{display: 'flex', flexDirection: 'Column'}}>
                <Button ><Link to="/">Categories</Link></Button>
                <Button ><Link to="/programs">Programs</Link></Button>
                <Button ><Link to="/bodyArea">Body Areas</Link></Button>
                <Button ><Link to="/products">Products</Link></Button>
              </Paper>
            </Grid>
          </Grid>
          <Grid xs={12} sm={8} md={9} style={{display: 'flex', flex: 1, justifyContent: 'center'}}>
            <Grid style={{width: '99%'}}>
              <Switch>
                <Paper elevation={4}>
                  <Route exact path="/" component={categories} />
                  {/*<Route exact path="/:new" component={categories} />*/}
                  <Route exact path="/addnew" component={Addnew} />
                  {/*<Route exact path="/edit" component={Addnew} />*/}
                  <Route exact path="/programs" component={programs} />
                  <Route exact path="/bodyArea" component={bodyArea} />
                  <Route exact path="/products" component={products} />
                </Paper>
              </Switch>
            </Grid>
          </Grid>
        </Grid>
      </Router>
    )
  }
}


export default App;