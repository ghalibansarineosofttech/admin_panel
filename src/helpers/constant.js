export class Constants{

    //base_url
    static baseURL = `#your Base Url`;

    //Regex's
    static nameReg = /^([a-zA-Z]{1,}([,]{1})?([ ])?([&]{1})?([ ])?[a-zA-Z]{1,})*[a-zA-Z]{1,}([ ])?[a-zA-Z,&]{1,}$/;
    static passwordReg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z-\d]{8,}$/;
    static emailReg = /^[a-zA-Z]{1,}([._-])?[a-zA-Z0-9]{1,}([._-])?[a-zA-Z0-9]{1,}([._-])?[a-zA-Z0-9]{1,}([._-])?[a-zA-Z0-9]{1,}([_-]{1})?[a-zA-Z0-9]{1,}[@]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,3}([.]{1}[a-zA-Z]{2})?$/;
    static numberReg = /^(([0-9]{1,}[,])?)*[0-9]{1,}$/;
    static hexReg = /^#[0-9A-F]{6}$/i;

    //Error message
    static blank = '';
    static required = 'Required';
    static nameErr = 'Only Alphabet is allowed.';
    static numberErr = 'only number is allowed.';
    static emailErr = 'Invalid email.';
    static hexErr = 'invalid hex value';
    static dobErr = 'Please Provide valid dob.';
    static passwordErr = 'password should have min 8 character, atleast 1 number and 1 alphabet.';
}